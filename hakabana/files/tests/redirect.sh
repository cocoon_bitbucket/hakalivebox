#!/usr/bin/env bash

# redirect tcp port 5000 to web server at web:80
echo "redirect port 5000 to web:80"
socat TCP4-LISTEN:5000,reuseaddr,fork TCP4:web:80 &

echo "redirect port 53 to dns server dns:53"
# redirect dns traffic to dns server
socat UDP4-RECVFROM:53,fork UDP4-SENDTO:dns:53 &

echo "redirection done..."
