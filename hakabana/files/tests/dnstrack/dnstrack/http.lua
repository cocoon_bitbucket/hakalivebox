-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.


local ipv4 = require('protocol/ipv4')
local tcp_connection = require('protocol/tcp_connection')
local http = require('protocol/http')

-- http.install_tcp_rule(80)

haka.rule{
	hook = haka.dissectors.http.events.request,
	eval = function (http, request)
        http.dataid = dnstrack:genid()
        dnstrack:insert('hakabana', 'http', http.dataid, {
            ['@timestamp'] = dsntrack:timestamp(haka.network_time()),
            uri = request.uri,
            ['user agent'] = request.headers['User-Agent'],
            ['host'] = request.headers['Host'],
            flow = http.flow.flowid
        })
	end
}

haka.rule{
	hook = haka.dissectors.http.events.response,
	eval = function (http, response)
		if not http.flow.ignore then
			dnstrack:update('hakabana', 'http', http.dataid, {
				status = response.status
			})
		end
	end
}


-- Log info about incoming ipv4 packets
haka.rule{
	-- Rule evaluated whenever a new ipv4 packet is received
	on = haka.dissectors.ipv4.events.receive_packet,
	-- Evauation function taking ipv4 packet structure
	-- as argument
	eval = function (pkt)
		-- All fields are accessible through pkt variable.
		-- See the Haka documentation for a complete list.
		haka.log("packet from %s to %s", pkt.src, pkt.dst)
	end
}

-- Log info about connection establsihments
haka.rule{
	-- Rule evaluated at connection establishment attempt
	on = haka.dissectors.tcp_connection.events.new_connection,
	-- Rule evaluated at connection establishment attempt
	eval = function (flow, tcp)
		-- Fields from previous layer are accessible too
		haka.log("TCP connection from %s:%d to %s:%d", tcp.src,
			tcp.srcport, tcp.dst, tcp.dstport)
		-- Raise a simple alert for testing purpose
		haka.alert{
			description = "A simple alert",
			severity = "low"
        }



	end
}