local es_host = "172.18.0.3"
local es_port = 9200
local elastricsearch = require('misc/elasticsearch')


hakabana = elastricsearch.connector('http://' .. es_host .. ':' .. es_port)
hakabana:newindex("hakabana", {
    mappings = {
        http = {
            properties = {
                ['user agent'] = {
                    type = 'string',
                    index = 'not_analyzed'
                },
                ['host'] = {
                    type = 'string',
                    index = 'not_analyzed'
                }
            }
        },
        dns = {
            properties = {
                ['query'] = {
                    type = 'string',
                    index = 'not_analyzed'
                }
            }
        }
    },
})


hakabana:insert('hakabana', 'dns', nil, {
				['@timestamp'] = hakabana:timestamp(os.time()),
				query = 'my query',
				flow = 1
			})

hakabana:insert('hakabana', 'http', hakabana:genid(), {
    ['@timestamp'] = hakabana:timestamp(os.time()),
    uri = 'my uri',
    ['user agent'] = 'my User-Agent',
    ['host'] = 'The Host',
    flow = 2
})








