#!/usr/bin/env bash

#echo "redirect port 53 to dns server dns:53"
# redirect dns traffic to dns server
#socat UDP4-RECVFROM:53,fork UDP4-SENDTO:dns:53 &


echo "start rsyslog "
service rsyslog restart

echo "start dns server unbound"
unbound &


echo "launch haka"
./run.lua

