

# start all
    docker-compose up

# find the elasticsearch ip:

    docker network inspect hakalivebox_default

* "Name": "hakalivebox_elasticsearch_1"
* "IPv4Address": "172.18.0.5/16"


# enter hakabana container

    docker-compose run -p 53:53/udp -p 5000:5000 hakabana bash

# edit haka.conf 
(to set  elasticsearch host)

    cd /app
    nano haka.conf

# launch it 
    ./dnsfilter



# test it
on another terminal

    dig orange.com @192.168.99.100

    dig asupspiciouslongdomainname.orange.com @192.168.99.100

# vizualise

in kibana

open browser at http://192.168.99.100:5601



in devtools enter query

    GET /_search
    {
        "query": {
            "query_string" : {
                "fields" : ["severity", "time"],
                "query" : "severity:low",
                "use_dis_max" : true
            }
        }
    }



should see something like:

    {
      "took": 62,
      "timed_out": false,
      "_shards": {
        "total": 6,
        "successful": 6,
        "failed": 0
      },
      "hits": {
        "total": 4,
        "max_score": 0.2876821,
        "hits": [
          {
            "_index": "ips",
            "_type": "alert",
            "_id": "LyrXUN4lIh3q7APT4C2pxA1",
            "_score": 0.2876821,
            "_source": {
              "time": "2017/03/28 13:31:28",
              "severity": "low",
              "description": "PDNS: orange.com. -> 185.63.192.20"
            }
          }, .../...


    



