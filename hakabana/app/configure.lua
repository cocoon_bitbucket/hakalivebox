#! /usr/bin/env lua

local socket=require('socket')
local t = require("template")

--local posix = require("posix")

template_filename= "haka.conf.txt"
configuration_filename= "haka.conf"



hostname=socket.dns.gethostname()

print(hostname)

self_ip= socket.dns.toip(hostname)
hakabana_ip= socket.dns.toip('hakabana')
redis_ip= socket.dns.toip('redis')
elasticsearch_ip= socket.dns.toip('elasticsearch')



-- Safe environmet to prevent templates
-- from doing things like running arbitary
-- commands with os.execute.
local env = {
    pairs  = pairs,
    ipairs = ipairs,
    type   = type,
    table  = table,
    string = string,
    date   = os.date,
    math   = math,
    adder  = adder,
    count  = count,

    self_ip= self_ip,
    redis_ip= redis_ip,
    elasticsearch_ip = elasticsearch_ip
}

-- build haka.conf content from template haka.conf.txt
conf= t.compile_file(template_filename, env)

-- open haka.conf
file= io.open (configuration_filename ,"w")
-- write configuration to it
file:write(conf)
file:close()


--print("redirect port 53 to dns server dns:53")
---- redirect dns traffic to dns server
--os.execute("socat UDP4-RECVFROM:53,fork UDP4-SENDTO:dns: &")
--
--
--print("launch haka")
--os.execute("haka -c haka.conf --no-daemon")



--local pid = posix.fork()
--
--if pid == 0 then
--
--  -- this is the child process
--  print(posix.getpid('pid') .. ": child socat process")
--  -- spawn socat process to redirect port 53
--  print("redirect port 53 to dns server dns:53")
--  -- redirect dns traffic to dns server
--  os.execute("socat UDP4-RECVFROM:53,fork UDP4-SENDTO:dns:53")
--
--else
--  -- this is the parent process
--  print(posix.getpid('pid') .. ": parent process haka ")
--
--  -- launch haka
--    os.execute("haka -c haka.conf --no-daemon")
--
--  -- wait for the child process to finish
--  -- posix.wait(pid)
--
--end
--
---- both processes get here
--print(posix.getpid('pid') .. ": quitting")





