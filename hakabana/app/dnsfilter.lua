-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

require("protocol/dns")

ipv4= require('protocol/ipv4')

geoip_module= require("misc/geoip")
local geoip = geoip_module.open('/usr/share/GeoIP/GeoIP.dat')
-- print(geoip:country(ipv4.addr("8.8.8.8")))
-- = US

local alert_name_size= 15
--require('misc/hakabana').initialize{
--	elasticsearch = {
--		host = "elasticsearch",
--		port = 9200
--	},
--    geoip_data = '/usr/share/GeoIP/GeoIP.dat'
--}

-- dns.install_udp_rule(53)

local function alert_pdns(array)
    for _, a in ipairs(array) do
        if a.type == "A" then
            haka.alert{
                description = string.format("PDNS: %s -> %s", a.name, a.ip),
                severity="low"
            }
        end
    end
end

haka.rule {
    on = haka.dissectors.dns.events.response,
    eval = function (dns, response)
        alert_pdns(response.answer)
        --alert_pdns(response.additional)
    end
}

haka.rule {
    on = haka.dissectors.dns.events.query,
    eval = function (dns, query)
        print("received dns query")
        haka.log('receive request for: ' .. query.question.name)
        if #query.question.name > alert_name_size then
            haka.alert{
                description = string.format("long dns query: %s", query.question.name),
                severity="medium"
            }
        end
        print("name: " .. query.question.name)
        print("type: " .. query.question.type)
        print("class: " .. query.question.class)
    end
}