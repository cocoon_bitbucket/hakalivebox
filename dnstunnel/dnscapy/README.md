
see: https://code.google.com/archive/p/dnscapy/

Software Requirements

Python >= 2.6
Scapy >= 2.1-dev (2.2 recommended)
Openssh
Linux (should work on Windows with some minor changes)
Note : once scapy is installed you have to patch a missing import. 
* Edit the file supersocket.py (located for example on /usr/local/lib/python2.6/dist-packages/scapy/supersocket.py) 
* Add the line: from scapy.packet import Padding