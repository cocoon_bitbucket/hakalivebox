#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dnslib import DNSRecord
from denise.core import Encoder

KEY = 'denise'
#DOMAINS = ['example.com']
DOMAINS = ['t.home.lan']

# address= "127.0.0.1"
# port= 8053

address= "192.168.99.100"

# direct access to dnstunnel
#port= 8054

# access via dns -> dnstunnel
port=8053

# access via haka -> dns -> dnstunnel
#port= 53


longdata = """Dies ist ein Test Text.

 Und der sollte sch�n lang sein, damit er kompremiert immernoch
 deutlich l�nger als 74 Zeichen ist und in mehr als ein Datenpaket
 kompremiert. Ob das jetzt reicht?

 """

data = longdata

encoder = Encoder(KEY, DOMAINS, channel=123)

request_packets = encoder.encodeDnsQuery(data, 0)

for packet in request_packets:
    sender = DNSRecord.parse(packet)
    a_pkt = sender.send(address, port)


print "Done"