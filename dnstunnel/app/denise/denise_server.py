# -*- coding: utf-8 -*-
from dnslib import *
from dnslib.server import DNSServer,DNSHandler,BaseResolver,DNSLogger

from core import Encoder,Decoder
from  core import FLAG_LASTFRAGMENT

from datastore import Datastore

KEY = 'denise'
#DOMAINS = ['example.com']
DOMAINS = ['t.home.lan']


class DomainName(str):
    def __getattr__(self, item):
        return DomainName(item + '.' + self)


# D = DomainName('example.com')
D =  DomainName('t.home.lan')
IP = '127.0.0.1'
TTL = 60 * 5
PORT = 5053

soa_record = SOA(
    mname=D.ns1,  # primary name server
    rname=D.andrei,  # email of the domain administrator
    times=(
        201307231,  # serial number
        60 * 60 * 1,  # refresh
        60 * 60 * 3,  # retry
        60 * 60 * 24,  # expire
        60 * 60 * 1,  # minimum
    )
)
ns_records = [NS(D.ns1), NS(D.ns2)]
records = {
    D: [A(IP), AAAA((0,) * 16), MX(D.mail), soa_record] + ns_records,
    D.ns1: [A(IP)],  # MX and NS records must never point to a CNAME alias (RFC 2181 section 10.3)
    D.ns2: [A(IP)],
    D.mail: [A(IP)],
    D.andrei: [CNAME(D)],
}

received_packets=[]
received_domains=[]

data_store= Datastore()

def dns_response(data):
    """


    :param data:
    :return:
    """
    request = DNSRecord.parse(data)

    print request

    reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)

    qname = request.q.qname
    qn = str(qname)
    qtype = request.q.qtype
    qt = QTYPE[qtype]

    # test: remove trailing .
    if qn.endswith('.'):
        qn= qn[:-1]

    if qn == D or qn.endswith('.' + D):

        for name, rrs in records.iteritems():
            if name == qn:
                for rdata in rrs:
                    rqt = rdata.__class__.__name__
                    if qt in ['*', rqt]:
                        #reply.add_answer(RR(rname=qname, rtype=QTYPE[rqt], rclass=1, ttl=TTL, rdata=rdata))
                        reply.add_answer(RR(rname=qname, rtype=QTYPE.reverse[rqt], rclass=1, ttl=TTL, rdata=rdata))

        # for rdata in ns_records:
        #     reply.add_ns(RR(rname=D, rtype=QTYPE.NS, rclass=1, ttl=TTL, rdata=rdata))
        #
        # reply.add_ns(RR(rname=D, rtype=QTYPE.SOA, rclass=1, ttl=TTL, rdata=soa_record))

    print "---- Reply:\n", reply

    return reply.pack()



def denise_response(data):
    """


    :param data: query dns packet
    :return:
    """
    global data_store


    request = DNSRecord.parse(data)
    #reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)
    qname = request.q.qname
    qn = str(qname)
    qtype = request.q.qtype
    qt = QTYPE[qtype]

    if qt != 'TXT':
        # this request is not denise request: reply with normal query
        return dns_response(data)

    # assume it is a denise tunnel sender

    last_fragment = False
    channel= request.header.id


    decoder = Decoder(KEY, DOMAINS)
    domains = [domain.qname.idna()[:-1] for domain in request.questions]

    domains_2 = []
    for s in domains:
        s = s[1:]
        domains_2.append(decoder.removeDomain(s.replace('.x', '')))

    for p in domains_2:
        payload, flags, serial = decoder.decodePacket(p)
        data_store.write(payload,flags,serial,channel=channel)
        #data_store[serial] = (payload, flags, serial)
        if flags & FLAG_LASTFRAGMENT == FLAG_LASTFRAGMENT:
            last_fragment = True

    if last_fragment:
        data= data_store.get(channel)
        if decoder.checkDataComplete(data):
            # message complete
            message= decoder.packetsToData(data)
            print message
            data_store.write_message(message,channel=channel)
            #
            data_store.clear(channel)
        else:
            # data not completed
            pass

    # return empty dns response
    reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)
    return reply



class DeniseResolver(BaseResolver):
    """
        decode denise packets

    """
    def __init__(self,key='denise',domains=None):
        """

        """
        # Parse RRs
        #self.rrs = RR.fromZone(zone)
        self._key= key
        self._domains= domains or ['t.home.lan']


    def resolve(self,request,handler):
        reply = request.reply()

        # Replace labels with request label
        # for rr in self.rrs:
        #     a = copy.copy(rr)
        #     a.rname = qname
        #     reply.add_answer(a)
        # return reply
        qname = request.q.qname
        qn = str(qname)
        qtype = request.q.qtype
        qt = QTYPE[qtype]

        if qt != 'TXT':
            return reply

        # assume it is a denise tunnel sender

        last_fragment = False
        channel = request.header.id

        decoder = Decoder(KEY, DOMAINS)
        domains = [domain.qname.idna()[:-1] for domain in request.questions]

        domains_2 = []
        for s in domains:
            s = s[1:]
            domains_2.append(decoder.removeDomain(s.replace('.x', '')))

        for p in domains_2:
            payload, flags, serial = decoder.decodePacket(p)
            data_store.write(payload, flags, serial, channel=channel)
            # data_store[serial] = (payload, flags, serial)
            if flags & FLAG_LASTFRAGMENT == FLAG_LASTFRAGMENT:
                last_fragment = True

        if last_fragment:
            data = data_store.get(channel)
            if decoder.checkDataComplete(data):
                # message complete
                message = decoder.packetsToData(data)
                print message
                data_store.write_message(message, channel=channel)
                #
                data_store.clear(channel)
            else:
                # data not completed
                pass

        # return empty dns response
        #reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)

        return reply




if __name__ == '__main__':


    from dnslib import DNSRecord
    import logging
    logging.basicConfig(level=logging.DEBUG)


    longdata = """Dies ist ein Test Text.

    Und der sollte sch�n lang sein, damit er kompremiert immernoch
    deutlich l�nger als 74 Zeichen ist und in mehr als ein Datenpaket
    kompremiert. Ob das jetzt reicht?

    """



    def test_dns_response():
        """

        :return:
        """
        # start the servers
        #servers= start_server()

        # build request
        request= DNSRecord.question(qname='mail.t.home.lan',qtype='A',qclass='IN')
        request_packet = request.pack()

        response_packet= dns_response(request_packet)
        response = DNSRecord.parse(response_packet)

        return

    def test_dns_server():
        """

            to test tne server:
                dig mail.example.com @127.0.0.1 -p 5053

            should respond:

                ;; ANSWER SECTION:
                mail.example.com.	300	IN	A	127.0.0.1

        :return:
        """
        # # start the servers
        # servers= start_server()
        #
        # wait_server(servers)

        return


    def test_denise():
        """



        :return:
        """
        # key= 'denise'
        # domains= ['example.com']
        # longdata = """Dies ist ein Test Text.
        #
        # Und der sollte sch�n lang sein, damit er kompremiert immernoch
        # deutlich l�nger als 74 Zeichen ist und in mehr als ein Datenpaket
        # kompremiert. Ob das jetzt reicht?
        #
        # """
        data= 'Test'
        data= longdata

        encoder= Encoder(KEY,DOMAINS)
        decoder= Decoder(KEY,DOMAINS)

        request_packets= encoder.encodeDnsQuery(data,0)
        decoded = decoder.decodeDnsQuery(*(request_packets,))
        assert decoded[0] == data

        # for request in request_packets:
        #     decoded= decoder.decodeDnsQuery((request,))
        #     assert decoded[0]==data
        #     continue


        return

    def test_denise_response():
        """



        :return:
        """

        data= 'Test'
        data= longdata

        encoder= Encoder(KEY,DOMAINS,channel=123)


        request_packets= encoder.encodeDnsQuery(data,0)


        for packet in request_packets:

            denise_response(packet)

        #decoded = decoder.decodeDnsQuery(*(request_packets,))
        #assert decoded[0] == data


        return


    def test_denise_server():
        """

        :return:
        """
        address= "127.0.0.1"
        port= 8053

        # start a denise server
        resolver = DeniseResolver()
        logger = DNSLogger("request,reply,truncated,error", False)

        udp_server = DNSServer(resolver,
                               port= port,
                               address= address,
                               logger=logger)

        udp_server.start_thread()


        # send denise payload to server
        data = 'Test'
        data = longdata

        encoder = Encoder(KEY, DOMAINS, channel=123)

        request_packets = encoder.encodeDnsQuery(data, 0)


        for packet in request_packets:

            sender= DNSRecord.parse(packet)
            a_pkt = sender.send(address, port)


        # while udp_server.isAlive():
        #     time.sleep(1)

        return

    #test_dns_response()
    #test_dns_server()
    #test_denise()

    #test_denise_response()
    test_denise_server()






    print "Done"
