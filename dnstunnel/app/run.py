#!/usr/bin/env python
# -*- coding: utf-8 -*-


import time

from dnslib.server import DNSServer,DNSLogger
from denise.denise_server import DeniseResolver

import logging
logging.basicConfig(level=logging.DEBUG)

address = "0.0.0.0"
port = 8053



# start a denise server
resolver = DeniseResolver()
logger = DNSLogger("request,reply,truncated,error", False)

udp_server = DNSServer(resolver,
                       port=port,
                       address=address,
                       logger=logger)

udp_server.start_thread()


while udp_server.isAlive():
    time.sleep(1)
