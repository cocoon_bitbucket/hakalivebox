# -*- coding: utf-8 -*-
"""
LICENSE http://www.apache.org/licenses/LICENSE-2.0
"""
import datetime
import sys
import time
import threading
import traceback
import SocketServer
from dnslib import *

from core import Encoder,Decoder
from  core import FLAG_LASTFRAGMENT

from datastore import Datastore

KEY = 'denise'
DOMAINS = ['example.com']



class DomainName(str):
    def __getattr__(self, item):
        return DomainName(item + '.' + self)


D = DomainName('example.com')
IP = '127.0.0.1'
TTL = 60 * 5
PORT = 5053

soa_record = SOA(
    mname=D.ns1,  # primary name server
    rname=D.andrei,  # email of the domain administrator
    times=(
        201307231,  # serial number
        60 * 60 * 1,  # refresh
        60 * 60 * 3,  # retry
        60 * 60 * 24,  # expire
        60 * 60 * 1,  # minimum
    )
)
ns_records = [NS(D.ns1), NS(D.ns2)]
records = {
    D: [A(IP), AAAA((0,) * 16), MX(D.mail), soa_record] + ns_records,
    D.ns1: [A(IP)],  # MX and NS records must never point to a CNAME alias (RFC 2181 section 10.3)
    D.ns2: [A(IP)],
    D.mail: [A(IP)],
    D.andrei: [CNAME(D)],
}

received_packets=[]
received_domains=[]

data_store= Datastore()


def dns_response(data):
    """


    :param data:
    :return:
    """
    request = DNSRecord.parse(data)

    print request

    reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)

    qname = request.q.qname
    qn = str(qname)
    qtype = request.q.qtype
    qt = QTYPE[qtype]

    # test: remove trailing .
    if qn.endswith('.'):
        qn= qn[:-1]

    if qn == D or qn.endswith('.' + D):

        for name, rrs in records.iteritems():
            if name == qn:
                for rdata in rrs:
                    rqt = rdata.__class__.__name__
                    if qt in ['*', rqt]:
                        #reply.add_answer(RR(rname=qname, rtype=QTYPE[rqt], rclass=1, ttl=TTL, rdata=rdata))
                        reply.add_answer(RR(rname=qname, rtype=QTYPE.reverse[rqt], rclass=1, ttl=TTL, rdata=rdata))

        # for rdata in ns_records:
        #     reply.add_ns(RR(rname=D, rtype=QTYPE.NS, rclass=1, ttl=TTL, rdata=rdata))
        #
        # reply.add_ns(RR(rname=D, rtype=QTYPE.SOA, rclass=1, ttl=TTL, rdata=soa_record))

    print "---- Reply:\n", reply

    return reply.pack()


def denise_response(data):
    """


    :param data: query dns packet
    :return:
    """
    global data_store


    request = DNSRecord.parse(data)
    #reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)
    qname = request.q.qname
    qn = str(qname)
    qtype = request.q.qtype
    qt = QTYPE[qtype]

    if qt != 'TXT':
        # this request is not denise request: reply with normal query
        return dns_response(data)

    # assume it is a denise tunnel sender

    last_fragment = False
    channel= request.header.id


    decoder = Decoder(KEY, DOMAINS)
    domains = [domain.qname.idna()[:-1] for domain in request.questions]

    domains_2 = []
    for s in domains:
        s = s[1:]
        domains_2.append(decoder.removeDomain(s.replace('.x', '')))

    for p in domains_2:
        payload, flags, serial = decoder.decodePacket(p)
        data_store.write(payload,flags,serial,channel=channel)
        #data_store[serial] = (payload, flags, serial)
        if flags & FLAG_LASTFRAGMENT == FLAG_LASTFRAGMENT:
            last_fragment = True

    if last_fragment:
        data= data_store.get(channel)
        if decoder.checkDataComplete(data):
            # message complete
            message= decoder.packetsToData(data)
            print message
            data_store.write_message(message,channel=channel)
            #
            data_store.clear(channel)
        else:
            # data not completed
            pass

    # return empty dns response
    reply = DNSRecord(DNSHeader(id=request.header.id, qr=1, aa=1, ra=1), q=request.q)
    return reply

    # decoded = decoder.decodeDnsQuery(*(received_packets,))
    # assert decoded[0] == data
    # request = DNSRecord.parse(data)











# def dns_handler(func,data):
#     def dns_handler_wrapper():
#         return func(data)
#     return dns_handler_wrapper


class BaseRequestHandler(SocketServer.BaseRequestHandler):
    """


    """
    _dns_handler= None

    def get_data(self):
        raise NotImplementedError

    def send_data(self, data):
        raise NotImplementedError

    def handle(self):
        now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')
        print "\n\n%s request %s (%s %s):" % (self.__class__.__name__[:3], now, self.client_address[0],
                                               self.client_address[1])
        try:
            data = self.get_data()
            print len(data), data.encode('hex')  # repr(data).replace('\\x', '')[1:-1]
            dns_handler= self._dns_handler or dns_response
            result= dns_handler(data)
            #handler= dns_handler(self._dns_handler,data)
            #handler()
            self.send_data(result)
        except Exception:
            traceback.print_exc(file=sys.stderr)


class TCPRequestHandler(BaseRequestHandler):

    def get_data(self):
        data = self.request.recv(8192).strip()
        sz = int(data[:2].encode('hex'), 16)
        if sz < len(data) - 2:
            raise Exception("Wrong size of TCP packet")
        elif sz > len(data) - 2:
            raise Exception("Too big TCP packet")
        return data[2:]

    def send_data(self, data):
        sz = hex(len(data))[2:].zfill(4).decode('hex')
        return self.request.sendall(sz + data)


class UDPRequestHandler(BaseRequestHandler):

    def get_data(self):
        return self.request[0].strip()

    def send_data(self, data):
        return self.request[1].sendto(data, self.client_address)



def start_server(udp_handler=UDPRequestHandler,tcp_handler=TCPRequestHandler,dns_handler=dns_response):
    """
        start a udp and tcp dns thread
    :return:
    """

    print "Starting nameserver..."

    servers = [
        SocketServer.ThreadingUDPServer(('', PORT), UDPRequestHandler),
        SocketServer.ThreadingTCPServer(('', PORT), TCPRequestHandler),
    ]
    for s in servers:
        s._dns_handler= dns_handler
        thread = threading.Thread(target=s.serve_forever)  # that thread will start one more thread for each request
        thread.daemon = True  # exit the server thread when the main thread terminates
        thread.start()
        print "%s server loop running in thread: %s" % (s.RequestHandlerClass.__name__[:3], thread.name)

    return servers

def wait_server(servers):
    """

    :return:
    """

    try:
        while 1:
            time.sleep(1)
            sys.stderr.flush()
            sys.stdout.flush()

    except KeyboardInterrupt:
        pass
    finally:
        for s in servers:
            s.shutdown()



if __name__ == '__main__':


    from dnslib import DNSRecord


    longdata = """Dies ist ein Test Text.

    Und der sollte sch�n lang sein, damit er kompremiert immernoch
    deutlich l�nger als 74 Zeichen ist und in mehr als ein Datenpaket
    kompremiert. Ob das jetzt reicht?

    """



    def test_dns_response():
        """

        :return:
        """
        # start the servers
        #servers= start_server()

        # build request
        request= DNSRecord.question(qname='mail.example.com',qtype='A',qclass='IN')
        request_packet = request.pack()

        response_packet= dns_response(request_packet)
        response = DNSRecord.parse(response_packet)

        return

    def test_dns_server():
        """

            to test tne server:
                dig mail.example.com @127.0.0.1 -p 5053

            should respond:

                ;; ANSWER SECTION:
                mail.example.com.	300	IN	A	127.0.0.1

        :return:
        """
        # start the servers
        servers= start_server()

        wait_server(servers)

        return


    def test_denise():
        """



        :return:
        """
        # key= 'denise'
        # domains= ['example.com']
        # longdata = """Dies ist ein Test Text.
        #
        # Und der sollte sch�n lang sein, damit er kompremiert immernoch
        # deutlich l�nger als 74 Zeichen ist und in mehr als ein Datenpaket
        # kompremiert. Ob das jetzt reicht?
        #
        # """
        data= 'Test'
        data= longdata

        encoder= Encoder(KEY,DOMAINS)
        decoder= Decoder(KEY,DOMAINS)

        request_packets= encoder.encodeDnsQuery(data,0)
        decoded = decoder.decodeDnsQuery(*(request_packets,))
        assert decoded[0] == data

        # for request in request_packets:
        #     decoded= decoder.decodeDnsQuery((request,))
        #     assert decoded[0]==data
        #     continue


        return

    def test_denise_response():
        """



        :return:
        """

        data= 'Test'
        data= longdata

        encoder= Encoder(KEY,DOMAINS,channel=123)


        request_packets= encoder.encodeDnsQuery(data,0)


        for packet in request_packets:

            denise_response(packet)

        #decoded = decoder.decodeDnsQuery(*(request_packets,))
        #assert decoded[0] == data


        return





    #test_dns_response()
    #test_dns_server()
    #test_denise()

    test_denise_response()


    print "Done"


