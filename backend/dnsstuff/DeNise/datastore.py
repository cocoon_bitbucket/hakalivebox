
database= {
    "datastore": {} ,
    "messages": {}
}

class AbstractDatastore(object):
    """


    """
    # def __init__(self,db=None):
    #     """
    #
    #
    #     :param redis_db:
    #     """



    def get(self,channel):
        """
            get data for this channel

        :param channel:
        :return: dict   { serial : [ payload, flags, serial ]
        """
        raise NotImplementedError

    def clear(self,channel):
        """

        :param channel:
        :return:
        """
        raise NotImplementedError

    def write(self,payload,flags,serial,channel=1):
        """

            data_store[serial] = (payload, flags, serial)

        :param chunk:
        :param channel:
        :return:
        """
        raise NotImplementedError



    def write_message(self,message,channel):
        """

        :param message:
        :param channel:
        :return:
        """
        raise NotImplementedError



class Datastore(AbstractDatastore):
    """


    """
    def __init__(self,db=None):
        """


        :param redis_db:
        """
        db= db or database
        self._datastore = db['datastore']
        self._messages = db['messages']

    def get(self,channel):
        """
            get data for this channel

        :param channel:
        :return: dict   { serial : [ payload, flags, serial ]
        """
        return self._datastore[channel]

    def clear(self,channel):
        """

        :param channel:
        :return:
        """
        try:
            self._datastore[channel]={}
        except KeyError:
            pass

    def write(self,payload,flags,serial,channel=1):
        """

            data_store[serial] = (payload, flags, serial)

        :param chunk:
        :param channel:
        :return:
        """
        try:
            data= self.get(channel)
        except KeyError:
            self._datastore[channel]={}
            data= self._datastore[channel]

        data[serial]= [payload,flags, serial]

        return True

    def write_message(self,message,channel):
        """

        :param message:
        :param channel:
        :return:
        """
        self._messages[channel]= message