local lapis = require("lapis")
local app = lapis.Application()


local prefix= "/myapp"

local redis_host = os.getenv("CERYX_REDIS_HOST") or "127.0.0.1"
local redis_port = os.getenv("CERYX_REDIS_PORT") or 6379
local redis_prefix = os.getenv("CERYX_REDIS_PREFIX") or "ceryx"
local redis = require "resty.redis"

local respond_to = require('lapis.application').respond_to
local to_json = require('lapis.util').to_json
local from_json = require('lapis.util').from_json


local redis_connect= function()

	local red = redis:new()
	red:set_timeout(100) -- 100 ms

	local res, err = red:connect(redis_host, redis_port)

	-- Return if could not connect to Redis
	if not res then
		--ngx.log("no redis connection")
	    return nil
	end
	--ngx.log("redis connection OK")
	return red
end

--[[
	sample value

	{"source":"my_source","target":"my_target"}
]]


app:get("/myapp/", function()
  return "Welcome to Lapis " .. require("lapis.version")
end)

app:get("/myapp/routes" , function(self)

	
	local red= redis_connect()

	local host= "dummy"
	local key = redis_prefix .. ":routes:" .. host

	res, err = red:get(key)
	if not res or res == ngx.null then
	    ngx.log(ngx.WARN,"cannot find key: "..key)
	    return "key not found: "..key
	end

	--ngx.log(ngx.NOTICE,"get redis key: "..key..", content: "..res)
	res= {}
    result= to_json(res)

 	return { json= result }

end)


app:match("host_route", "/myapp/routes/:host" , respond_to( {
	-- get a route to a host
	GET= function(self)

		local red= redis_connect()

		local host= self.params.host
		local key = redis_prefix .. ":routes:" .. host

		res, err = red:get(key)
		if not res or res == ngx.null then
		    ngx.log(ngx.WARN,"cannot find key: "..key)
		    return "key not found: "..key
		end

		ngx.log(ngx.NOTICE,"get redis key: "..key..", content: "..res)
		
	    result= from_json(res)
	 	return { json= result }

	end,
	POST= function(self)

		local red= redis_connect()

		local host= self.params.host
		local route= self.params.route

		local key = redis_prefix .. ":routes:" .. host

		res, err = red:set(key,route)

		if not res or res == ngx.null then
		    ngx.log(ngx.WARN,"cannot set key: "..key)
		    return "cannot set key "..key
		end

		ngx.log(ngx.NOTICE,"set redis key: "..key..", content: "..route)
		
	 	--return { redirect_to= self:url_for("index")}
	 	return "host added"
	end,

	DELETE= function(self)

		local red= redis_connect()

		local host= self.params.host
		local key = redis_prefix .. ":routes:" .. host

		res, err = red:delete(key)
		if not res or res == ngx.null then
		    ngx.log(ngx.WARN,"cannot delete key: "..key)
		    return "key deleted: "..key
		end

		ngx.log(ngx.NOTICE,"get redis key: "..key..", content: "..res)
		
	    result= {}
	 	return { json= result }

	end




 }))



return app

