
testing openresty


docker image
============

build cocoon/resty image
------------------------
    docker build -t cocoon/resty .


launch application with
-----------------------
    docker-compose up

2 containers are started
* alpineresty_proxy_1 
* alpineresty_redis_1 


see main app
------------
open a browser at  192.168.99.100

or 
    wget 192.168.99.100

you shoukd see
    Something. 


test lapis myapp in debug mod
-----------------------------

open a terminal on the conatainer alpineresty_proxy_1 

and start myapp 

    cd /opt/openresty/nginx/lualib/myapp
    lapis server
open a  brower at 192.168.99.100:8080/myapp/
or
    wget 192.168.99.100:8080/myapp/

you shoud see
    Welcome to Lapis 1.4.3

