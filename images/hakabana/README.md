

build the image
================

    docker build -t cocoon/hakabana .



run it
======

    docker run -ti cocoon/hakabana

 test haka
 ----------

     cd /usr/local/share/haka/sample/hellopacket
     hakapcap hellopacket.lua hellopacket.pcap


should output:

	root@528f2d0b3697:/usr/local/share/haka/sample/hellopacket# hakapcap hellopacket.lua hellopacket.pcap
	info  core: load module 'capture/pcap.so', Pcap Module
	info  core: load module 'alert/file.so', File alert
	info  core: setting packet capture mode to pass-through

	info  core: loading rule file 'hellopacket.lua'
	info  core: initializing thread 0
	info  dissector: register new dissector 'packet'
	info  core:      register policy 'unknown dissector' on 'packet next dissector'
	info  core:      register policy 'unknown protocol' on 'packet next dissector'
	info  capture:   opening file 'hellopacket.pcap'
	info  dissector: register new dissector 'ipv4'
	info  core:      register policy 'ipv4' on 'packet next dissector'
	info  dissector: register new dissector 'tcp'
	info  core:      register policy 'tcp' on 'ipv4 next dissector'
	info  dissector: register new dissector 'tcp_connection'
	info  core:      register policy 'default action' on 'no connection found for tcp packet'
	info  core:      register policy 'default action' on 'unexpected tcp packet'
	info  core:      register policy 'default action' on 'invalid tcp handshake'
	info  core:      register policy 'tcp connection' on 'tcp next dissector'
	info  core:      1 rule(s) on event 'ipv4:receive_packet'
	info  core:      2 rule(s) on event 'tcp_connection:new_connection'
	info  core:      3 rule(s) registered

	info  core:      starting single threaded processing

	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  TCP connection from 192.168.10.1:47161 to 192.168.10.99:3000
	alert: id = 1
		time = Sun Sep 18 21:33:55 2016
		severity = low
		description = A simple alert
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  core:      unload module 'Pcap Module'
	info  core:      unload module 'File alert'



test hakabana server
--------------------

adapt config:  /usr/local/share/haka/hakabana/config.lua

    haka -c /usr/local/share/haka/hakabana/haka.conf

Visit kibana webpage at http://<webserver-address>/kibana


LUA_PATH=<LDIR>/?.lua;?.lua
LUA_CPATH=<CDIR>/?.so;?.so



build standalone luajit interpreter (optional)
-----------------------------------------------
    cd /tmp/haka/external/luajit/src && make && make install
    cd /usr/local/bin && ln -s luajit-2.0.4 lua
    
install luarocks (optional)
----------------
    cd /usr/local/bin && ln -s luajit-2.0.4 lua
    git clone git://github.com/luarocks/luarocks.git
    cd luarocks
    ./configure --with-lua=/usr/local --with-lua-include=/tmp/haka/external/luajit/src/src
    make build
    make install
    luarocks install redis-lua

