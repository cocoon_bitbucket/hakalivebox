local dns = require('protocol/dns')

-- dns.install_udp_rule(53)

haka.rule{
	hook = dns.events.query,
	eval = function (dns, query)
		if not dns.flow.ignore then
			hakadns:insert('hakabana', 'dns', nil, {
				['@timestamp'] = hakadns:timestamp(haka.network_time()),
				query = query.question['name'],
				flow = dns.flow.flowid
			})
		end
	end
}