-- /usr/local/share/haka/hakadns/config.lua


require('hakadns/init').initialize{
	elasticsearch = {
		host = "elasticsearch",
		port = 9200
	},
	geoip_data = '/usr/share/GeoIP/GeoIP.dat'
}

