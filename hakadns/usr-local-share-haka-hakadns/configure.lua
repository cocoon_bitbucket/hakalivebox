#! /usr/bin/env lua

local socket=require('socket')
local t = require("template")

--local posix = require("posix")

template_filename= "haka.conf.txt"
configuration_filename= "haka.conf"



hostname=socket.dns.gethostname()

print(hostname)

self_ip= socket.dns.toip(hostname)
redis_ip= socket.dns.toip('redis')
elasticsearch_ip= socket.dns.toip('elasticsearch')



-- Safe environmet to prevent templates
-- from doing things like running arbitary
-- commands with os.execute.
local env = {
    pairs  = pairs,
    ipairs = ipairs,
    type   = type,
    table  = table,
    string = string,
    date   = os.date,
    math   = math,
    adder  = adder,
    count  = count,

    self_ip= self_ip,
    redis_ip= redis_ip,
    elasticsearch_ip = elasticsearch_ip
}

-- build haka.conf content from template haka.conf.txt
conf= t.compile_file(template_filename, env)

-- open haka.conf
file= io.open (configuration_filename ,"w")
-- write configuration to it
file:write(conf)
file:close()





