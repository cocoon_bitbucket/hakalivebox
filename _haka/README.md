build the image
---------------

    docker build -t haka .


test the image
--------------

    docker run -ti haka bash

build the luajit interpreter
----------------------------
cd /usr/local/src/haka/external/luajit/src
make
make install

=> /usr/local/bin/luajit

 test haka
 ----------

     cd /usr/local/share/haka/sample/hellopacket
     hakapcap hellopacket.lua hellopacket.pcap


should output:

	root@528f2d0b3697:/usr/local/share/haka/sample/hellopacket# hakapcap hellopacket.lua hellopacket.pcap
	info  core: load module 'capture/pcap.so', Pcap Module
	info  core: load module 'alert/file.so', File alert
	info  core: setting packet capture mode to pass-through

	info  core: loading rule file 'hellopacket.lua'
	info  core: initializing thread 0
	info  dissector: register new dissector 'packet'
	info  core:      register policy 'unknown dissector' on 'packet next dissector'
	info  core:      register policy 'unknown protocol' on 'packet next dissector'
	info  capture:   opening file 'hellopacket.pcap'
	info  dissector: register new dissector 'ipv4'
	info  core:      register policy 'ipv4' on 'packet next dissector'
	info  dissector: register new dissector 'tcp'
	info  core:      register policy 'tcp' on 'ipv4 next dissector'
	info  dissector: register new dissector 'tcp_connection'
	info  core:      register policy 'default action' on 'no connection found for tcp packet'
	info  core:      register policy 'default action' on 'unexpected tcp packet'
	info  core:      register policy 'default action' on 'invalid tcp handshake'
	info  core:      register policy 'tcp connection' on 'tcp next dissector'
	info  core:      1 rule(s) on event 'ipv4:receive_packet'
	info  core:      2 rule(s) on event 'tcp_connection:new_connection'
	info  core:      3 rule(s) registered

	info  core:      starting single threaded processing

	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  TCP connection from 192.168.10.1:47161 to 192.168.10.99:3000
	alert: id = 1
		time = Sun Sep 18 21:33:55 2016
		severity = low
		description = A simple alert
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  external:  packet from 192.168.10.99 to 192.168.10.1
	info  external:  packet from 192.168.10.1 to 192.168.10.99
	info  core:      unload module 'Pcap Module'
	info  core:      unload module 'File alert'



problem: 
========
root@f14be6f69fca:/usr/local/share/haka/sample/hellopacket# hakapcap
hakapcap: error while loading shared libraries: libhaka.so.0: cannot open shared object file: No such file or directory

hint at : https://travis-ci.org/haka-security/haka/builds/136081325

10 months ago

{
  "language": "c",
  "compiler": "gcc",
  "install": [
    "wget http://heanet.dl.sourceforge.net/project/swig/swig/swig-2.0.11/swig-2.0.11.tar.gz",
    "tar xzf swig-2.0.11.tar.gz",
    "cd swig-2.0.11/ && ./configure --prefix=/usr && make && sudo make install",
    "cd ../",
    "sudo apt-get update -qq",
    "sudo apt-get install -y tshark check rsync libpcap-dev gawk libedit-dev libpcre3-dev uuid-dev valgrind",
    "mkdir workspace",
    "cd workspace",
    "cmake ../"
  ],
  "script": "CTEST_OUTPUT_ON_FAILURE=1 make tests",
  "group": "stable",
  "dist": "precise",
  "os": "linux"
}
